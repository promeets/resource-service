FROM openjdk:alpine
ADD target/*.jar app.jar
RUN sh -c 'touch /app.jar'
ENV PROFILE default
EXPOSE 8080
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-Dspring.profiles.active=${PROFILE}","-jar","/app.jar","com.promeets.service.resource.ResourceApplication"]

