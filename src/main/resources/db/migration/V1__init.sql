CREATE TABLE "users" (
	"user_id" serial NOT NULL,
	"email" TEXT NOT NULL UNIQUE,
	"password" TEXT NOT NULL,
	"first_name" TEXT NOT NULL,
	"last_name" TEXT NOT NULL,
	CONSTRAINT users_pk PRIMARY KEY ("user_id")
) WITH (
OIDS=FALSE
);



CREATE TABLE "organizations" (
	"organization_id" bigserial NOT NULL,
	"name" TEXT NOT NULL UNIQUE,
	CONSTRAINT organizations_pk PRIMARY KEY ("organization_id")
) WITH (
OIDS=FALSE
);



CREATE TABLE "users_organizations" (
	"user_id" bigint NOT NULL,
	"organization_id" bigint NOT NULL,
	"role_id" bigint NOT NULL,
	"id" bigserial NOT NULL UNIQUE,
	CONSTRAINT users_organizations_pk PRIMARY KEY ("id")
) WITH (
OIDS=FALSE
);



CREATE TABLE "projects" (
	"project_id" bigserial NOT NULL,
	"name" TEXT NOT NULL,
	"description" TEXT,
	"organization_id" bigint NOT NULL,
	CONSTRAINT projects_pk PRIMARY KEY ("project_id")
) WITH (
OIDS=FALSE
);



CREATE TABLE "users_projects" (
	"user_id" bigint NOT NULL,
	"project_id" bigint NOT NULL,
	"role_id" bigint NOT NULL,
	"id" bigserial NOT NULL UNIQUE,
	CONSTRAINT users_projects_pk PRIMARY KEY ("id")
) WITH (
OIDS=FALSE
);



CREATE TABLE "tasks" (
	"task_id" bigserial NOT NULL,
	"name" TEXT NOT NULL,
	"project_id" bigint NOT NULL,
	"assignee_id" bigint,
	"reporter_id" bigint NOT NULL,
	"priority_id" bigint NOT NULL,
	"status_id" bigint NOT NULL,
	"description" TEXT,
	"due_date" TIMESTAMPTZ,
	"created_at" TIMESTAMPTZ NOT NULL,
	CONSTRAINT tasks_pk PRIMARY KEY ("task_id")
) WITH (
OIDS=FALSE
);

CREATE TABLE "task_priorities" (
	"priority_id" bigserial NOT NULL,
	"name" TEXT NOT NULL,
	CONSTRAINT task_priorities_pk PRIMARY KEY ("priority_id")
) WITH (
OIDS=FALSE
);

CREATE TABLE "task_statuses" (
	"status_id" bigserial NOT NULL,
	"name" TEXT NOT NULL,
	CONSTRAINT task_statuses_pk PRIMARY KEY ("status_id")
) WITH (
OIDS=FALSE
);



CREATE TABLE "security_rights" (
	"right_id" bigserial NOT NULL,
	"name" TEXT NOT NULL UNIQUE,
	CONSTRAINT security_rights_pk PRIMARY KEY ("right_id")
) WITH (
OIDS=FALSE
);



CREATE TABLE "security_roles" (
	"role_id" bigserial NOT NULL,
	"name" TEXT NOT NULL UNIQUE,
	CONSTRAINT security_roles_pk PRIMARY KEY ("role_id")
) WITH (
OIDS=FALSE
);



CREATE TABLE "users_tasks" (
	"user_id" bigint NOT NULL,
	"task_id" bigint NOT NULL,
	"role_id" bigint NOT NULL,
	"id" bigserial NOT NULL UNIQUE,
	CONSTRAINT users_tasks_pk PRIMARY KEY ("id")
) WITH (
OIDS=FALSE
);



CREATE TABLE "roles_rights" (
	"role_id" bigint NOT NULL,
	"right_id" bigint NOT NULL,
	CONSTRAINT roles_rights_pk PRIMARY KEY ("role_id","right_id")
) WITH (
OIDS=FALSE
);





ALTER TABLE "users_organizations" ADD CONSTRAINT "users_organizations_fk0" FOREIGN KEY ("user_id") REFERENCES "users"("user_id");
ALTER TABLE "users_organizations" ADD CONSTRAINT "users_organizations_fk1" FOREIGN KEY ("organization_id") REFERENCES "organizations"("organization_id");
ALTER TABLE "users_organizations" ADD CONSTRAINT "users_organizations_fk2" FOREIGN KEY ("role_id") REFERENCES "security_roles"("role_id");

ALTER TABLE "projects" ADD CONSTRAINT "projects_fk0" FOREIGN KEY ("organization_id") REFERENCES "organizations"("organization_id");

ALTER TABLE "users_projects" ADD CONSTRAINT "users_projects_fk0" FOREIGN KEY ("user_id") REFERENCES "users"("user_id");
ALTER TABLE "users_projects" ADD CONSTRAINT "users_projects_fk1" FOREIGN KEY ("project_id") REFERENCES "projects"("project_id");
ALTER TABLE "users_projects" ADD CONSTRAINT "users_projects_fk2" FOREIGN KEY ("role_id") REFERENCES "security_roles"("role_id");

ALTER TABLE "tasks" ADD CONSTRAINT "tasks_fk0" FOREIGN KEY ("project_id") REFERENCES "projects"("project_id");
ALTER TABLE "tasks" ADD CONSTRAINT "tasks_fk1" FOREIGN KEY ("assignee_id") REFERENCES "users"("user_id");
ALTER TABLE "tasks" ADD CONSTRAINT "tasks_fk2" FOREIGN KEY ("reporter_id") REFERENCES "users"("user_id");
ALTER TABLE "tasks" ADD CONSTRAINT "tasks_fk3" FOREIGN KEY ("priority_id") REFERENCES "task_priorities"("priority_id");
ALTER TABLE "tasks" ADD CONSTRAINT "tasks_fk4" FOREIGN KEY ("status_id") REFERENCES "task_statuses"("status_id");

ALTER TABLE "users_tasks" ADD CONSTRAINT "users_tasks_fk0" FOREIGN KEY ("user_id") REFERENCES "users"("user_id");
ALTER TABLE "users_tasks" ADD CONSTRAINT "users_tasks_fk1" FOREIGN KEY ("task_id") REFERENCES "tasks"("task_id");
ALTER TABLE "users_tasks" ADD CONSTRAINT "users_tasks_fk2" FOREIGN KEY ("role_id") REFERENCES "security_roles"("role_id");

ALTER TABLE "roles_rights" ADD CONSTRAINT "roles_rights_fk0" FOREIGN KEY ("role_id") REFERENCES "security_roles"("role_id");
ALTER TABLE "roles_rights" ADD CONSTRAINT "roles_rights_fk1" FOREIGN KEY ("right_id") REFERENCES "security_rights"("right_id");

