INSERT INTO security_roles (role_id, name) VALUES (1, 'ROLE_MEMBER');
INSERT INTO security_roles (role_id, name) VALUES (2, 'ROLE_OWNER');

INSERT INTO security_rights (right_id, name) VALUES (1, 'READ');
INSERT INTO security_rights (right_id, name) VALUES (2, 'WRITE');

INSERT INTO roles_rights (role_id, right_id) VALUES (1, 1);
INSERT INTO roles_rights (role_id, right_id) VALUES (2, 1);
INSERT INTO roles_rights (role_id, right_id) VALUES (2, 2);

INSERT INTO task_priorities (priority_id, name) VALUES (1, 'Critical');
INSERT INTO task_priorities (priority_id, name) VALUES (2, 'High');
INSERT INTO task_priorities (priority_id, name) VALUES (3, 'Normal');
INSERT INTO task_priorities (priority_id, name) VALUES (4, 'Low');

INSERT INTO task_statuses (status_id, name) VALUES (1, 'Open');
INSERT INTO task_statuses (status_id, name) VALUES (2, 'In progress');
INSERT INTO task_statuses (status_id, name) VALUES (3, 'On hold');
INSERT INTO task_statuses (status_id, name) VALUES (4, 'Closed');
INSERT INTO task_statuses (status_id, name) VALUES (5, 'Reopened');

/*password: john */
INSERT INTO users (user_id, email, password, first_name, last_name)
VALUES (1, 'john@mail.com', '$2a$04$g9gkoux2/pjPR7tOZKrj/OdGiRE5s5uq/WYos56N.BKWNAKlzKUTi', 'John', 'Smith');

/*password: sara */
INSERT INTO users (user_id, email, password, first_name, last_name)
VALUES (2, 'sara@mail.com', '$2a$04$IyERrDlfenYpTCoLbYDvxuJs991XRAErC9mlKaBUrx1qVDyix6xVW', 'Sara', 'Connor');

INSERT INTO users (user_id, email, password, first_name, last_name)
VALUES (3, 'michiael@mail.com', 'michael', 'Michael', 'Jennet');

INSERT INTO organizations (organization_id, name)
VALUES (1, 'Promeets');

INSERT INTO organizations (organization_id, name)
VALUES (2, 'Google');

INSERT INTO users_organizations (user_id, organization_id, role_id)
VALUES (1, 1, 1);

INSERT INTO users_organizations (user_id, organization_id, role_id)
VALUES (2, 1, 2);

INSERT INTO users_organizations (user_id, organization_id, role_id)
VALUES (3, 2, 2);

INSERT INTO projects (project_id, name, organization_id, description)
VALUES (12030921, 'Apollo', 1, 'Description of the Apollo project');

INSERT INTO users_projects (user_id, project_id, role_id)
VALUES (1, 12030921, 1);

INSERT INTO users_projects (user_id, project_id, role_id)
VALUES (2, 12030921, 1);

INSERT INTO tasks (task_id, NAME, project_id, assignee_id, reporter_id, description,
        priority_id, status_id, due_date, created_at)
VALUES (1, 'Start project', 12030921, 1, 2,
        'Start new project with particular folder structure and push it into remote git repository.',
        2,2,to_timestamp('23-08-2017 15:00', 'dd-mm-yyyy hh24:mi'), now());

INSERT INTO users_tasks (user_id, task_id, role_id)
VALUES (1, 1, 2);

INSERT INTO users_tasks (user_id, task_id, role_id)
VALUES (2, 1, 2);