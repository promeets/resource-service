package com.promeets.service.resource.model.entity.project;

import com.promeets.service.resource.model.entity.organization.Organization;
import com.promeets.service.resource.model.entity.organization.UserOrganization;
import com.promeets.service.resource.model.entity.task.ProjectTask;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "projects")
public class Project
{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long projectId;

    @Column
    private String name;

    @Column
    private String description;

    @ManyToOne
    @JoinColumn(name = "organization_id")
    private Organization organization;

    @OneToMany(mappedBy = "project")
    private List<ProjectTask> tasks;

    @OneToMany(mappedBy = "project")
    private List<UserProject> users;

}
