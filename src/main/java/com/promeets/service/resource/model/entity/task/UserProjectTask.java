package com.promeets.service.resource.model.entity.task;

import com.promeets.service.resource.model.entity.user.AbstractUserAssociation;
import lombok.*;

import javax.persistence.*;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table(name = "users_tasks")
@ToString(exclude = "task")
public class UserProjectTask extends AbstractUserAssociation
{
    @ManyToOne
    @JoinColumn(name = "task_id")
    private ProjectTask task;
}
