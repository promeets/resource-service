package com.promeets.service.resource.model.service.security.impl;

import com.promeets.service.resource.model.service.security.UserOrganizationSecurityService;
import com.promeets.service.resource.model.service.security.UserSecurityService;
import com.promeets.service.resource.model.entity.security.SecurityRight;
import com.promeets.service.resource.model.entity.security.SecurityRights;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service(value = "userOrganizationSecurityService")
public class UserOrganizationSecurityServiceImpl implements UserOrganizationSecurityService {
    private final UserSecurityService userSecurityService;

    @Autowired
    public UserOrganizationSecurityServiceImpl(UserSecurityService userSecurityService) {
        this.userSecurityService = userSecurityService;
    }

    public boolean canRead(long organizationId) {
        return userSecurityService.currentUser()
                .getOrganizations().stream()
                .map(organization ->
                {
                    if (organization.getOrganization().getOrganizationId() == organizationId) {
                        return organization.getRole().getRights();
                    }
                    return Collections.<SecurityRight>emptyList();
                })
                .anyMatch(SecurityRights.READ::hasRight);
    }
}
