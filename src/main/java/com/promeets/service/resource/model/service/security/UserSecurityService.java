package com.promeets.service.resource.model.service.security;

import com.promeets.service.resource.model.entity.user.User;

public interface UserSecurityService
{
    User currentUser();
}
