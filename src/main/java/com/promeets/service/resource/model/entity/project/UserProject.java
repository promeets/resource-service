package com.promeets.service.resource.model.entity.project;

import com.promeets.service.resource.model.entity.user.AbstractUserAssociation;
import lombok.*;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table(name = "users_projects")
@ToString(exclude = "project")
public class UserProject extends AbstractUserAssociation {

    @ManyToOne
    @JoinColumn(name = "project_id")
    private Project project;

}
