package com.promeets.service.resource.model.entity.security;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "security_rights")
public class SecurityRight {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long rightId;

    @Column
    private String name;

}
