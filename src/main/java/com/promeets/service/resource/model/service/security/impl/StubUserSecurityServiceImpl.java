package com.promeets.service.resource.model.service.security.impl;

import com.promeets.service.resource.model.service.security.UserSecurityService;
import com.promeets.service.resource.model.entity.user.User;
import com.promeets.service.resource.repository.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
public class StubUserSecurityServiceImpl implements UserSecurityService {
    private final UserRepository userRepository;

    @Autowired
    public StubUserSecurityServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User currentUser() {
        return userRepository.findByEmail(
                SecurityContextHolder
                        .getContext()
                        .getAuthentication()
                        .getName()
        ).orElse(userRepository
                .findByEmail("sara@mail.com")
                .orElse(userRepository.findOne((long) 1))
        );
    }
}
