package com.promeets.service.resource.model.entity.task;


import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "task_statuses")
public class ProjectTaskStatus {
    @Id
    @Column
    @GeneratedValue
    private long statusId;

    @Column
    private String name;
}