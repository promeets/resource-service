package com.promeets.service.resource.model.service.security.impl;

import com.promeets.service.resource.model.entity.user.User;
import com.promeets.service.resource.model.service.security.UserSecurityService;
import com.promeets.service.resource.repository.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Primary
@Profile("security")
@Service
public class UserSecurityServiceImpl implements UserSecurityService {
    private final UserRepository userRepository;

    @Autowired
    public UserSecurityServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User currentUser() {
        return userRepository.findByEmail(
                SecurityContextHolder
                        .getContext()
                        .getAuthentication()
                        .getName()
        ).orElseThrow(() ->
                new IllegalStateException("Unable find current authenticated user by authentication name.")
        );
    }
}
