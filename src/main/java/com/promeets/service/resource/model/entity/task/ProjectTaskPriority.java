package com.promeets.service.resource.model.entity.task;


import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "task_priorities")
public class ProjectTaskPriority {
    @Id
    @Column
    @GeneratedValue
    private long priorityId;

    @Column
    private String name;
}
