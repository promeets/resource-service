package com.promeets.service.resource.model.service.security;

public interface UserOrganizationSecurityService
{
    boolean canRead(long organizationId);
}
