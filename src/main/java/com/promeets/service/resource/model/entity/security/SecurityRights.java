package com.promeets.service.resource.model.entity.security;

import java.util.Collection;

/**
 * This class contains all predefined security rights from database
 */
public enum SecurityRights {
    READ,
    WRITE;


    public boolean hasRight(Collection<SecurityRight> rights) {
        return rights.stream()
                .filter(securityRight -> securityRight.getName().equals(this.name()))
                .count() > 0;
    }

    public boolean hasRight(SecurityRight right) {
        return right.getName().equals(this.name());
    }
}
