package com.promeets.service.resource.model.entity.user;

import com.promeets.service.resource.model.entity.security.SecurityRole;
import lombok.Data;
import lombok.ToString;

import javax.persistence.*;

@Data
@MappedSuperclass
@ToString(exclude = {"user"})
public abstract class AbstractUserAssociation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ManyToOne
    @JoinColumn(name = "user_id")
    protected User user;

    @ManyToOne
    @JoinColumn(name = "role_id")
    protected SecurityRole role;

}