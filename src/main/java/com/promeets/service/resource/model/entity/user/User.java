package com.promeets.service.resource.model.entity.user;

import com.promeets.service.resource.model.entity.organization.UserOrganization;
import com.promeets.service.resource.model.entity.project.UserProject;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table(name = "users")
@ToString(exclude = {"organizations", "projects"})
public class User extends AbstractUser implements Serializable {

    @Column(unique = true)
    private String email;

    @Column
    private String firstName;

    @Column
    private String lastName;

    @OneToMany(mappedBy = "user")
    private List<UserOrganization> organizations;

    @OneToMany(mappedBy = "user")
    private List<UserProject> projects;

}
