package com.promeets.service.resource.model.entity.security;

/**
 * This class contains all predefined security roles from database
 */
public enum SecurityRoles {
    ROLE_OWNER,
    ROLE_MEMBER;
}
