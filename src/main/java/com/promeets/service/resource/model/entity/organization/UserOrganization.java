package com.promeets.service.resource.model.entity.organization;

import com.promeets.service.resource.model.entity.user.AbstractUserAssociation;
import lombok.*;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table(name = "users_organizations")
@ToString(exclude = {"organization"})
public class UserOrganization extends AbstractUserAssociation {

    @ManyToOne
    @JoinColumn(name = "organization_id")
    private Organization organization;

}
