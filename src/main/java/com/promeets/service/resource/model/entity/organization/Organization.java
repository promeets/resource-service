package com.promeets.service.resource.model.entity.organization;

import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "organizations")
@ToString(exclude = "employees")
public class Organization {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long organizationId;

    @Column
    private String name;

    @OneToMany(mappedBy = "organization")
    private List<UserOrganization> employees;

}
