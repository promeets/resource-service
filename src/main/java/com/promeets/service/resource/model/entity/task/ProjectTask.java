package com.promeets.service.resource.model.entity.task;

import com.promeets.service.resource.model.entity.project.Project;
import com.promeets.service.resource.model.entity.user.User;
import lombok.Data;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;

@Data
@Entity
@Table(name = "tasks")
public class ProjectTask
{
    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long taskId;

    @Column
    private String name;

    @Column
    private String description;

    @Column
    private Timestamp createdAt;

    @Column
    private Timestamp dueDate;

    @ManyToOne
    @JoinColumn(name = "priority_id")
    private ProjectTaskPriority taskPriority;

    @ManyToOne
    @JoinColumn(name = "status_id")
    private ProjectTaskStatus taskStatus;

    @ManyToOne
    @JoinColumn(name = "project_id")
    private Project project;

    @ManyToOne
    @JoinColumn(name = "reporter_id")
    private User reporter;

    @ManyToOne
    @JoinColumn(name = "assignee_id")
    private User assignee;
}
