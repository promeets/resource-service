package com.promeets.service.resource.repository.organization;

import com.promeets.service.resource.model.entity.organization.Organization;
import com.promeets.service.resource.model.entity.organization.UserOrganization;
import com.promeets.service.resource.model.entity.security.SecurityRoles;
import com.promeets.service.resource.repository.security.SecurityRoleRepository;
import com.promeets.service.resource.repository.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.core.annotation.*;
import org.springframework.stereotype.Component;

@Component
@RepositoryEventHandler
public class OrganizationEventHandler {
    private final UserRepository userRepository;
    private final UserOrganizationRepository userOrganizationRepository;
    private final SecurityRoleRepository securityRoleRepository;

    @Autowired
    public OrganizationEventHandler(SecurityRoleRepository securityRoleRepository, UserRepository userRepository, UserOrganizationRepository userOrganizationRepository) {
        this.securityRoleRepository = securityRoleRepository;
        this.userRepository = userRepository;
        this.userOrganizationRepository = userOrganizationRepository;
    }

    @HandleAfterCreate
    public void handleAfterCreate(Organization organization) {
        UserOrganization userOrganization = new UserOrganization();
        userOrganization.setUser(userRepository.me());
        userOrganization.setOrganization(organization);
        userOrganization.setRole(securityRoleRepository.findByName(SecurityRoles.ROLE_OWNER.name()));
        userOrganizationRepository.save(userOrganization);
    }

}
