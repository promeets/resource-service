package com.promeets.service.resource.repository.project;

import com.promeets.service.resource.model.entity.project.Project;
import org.springframework.data.repository.CrudRepository;

public interface ProjectRepository extends CrudRepository<Project, Long> {
}
