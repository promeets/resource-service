package com.promeets.service.resource.repository.project;

import com.promeets.service.resource.model.entity.project.UserProject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

@Projection(name="inlineProject", types = {UserProject.class})
public interface InlineProjectProjection {
    @Value("#{target.project.projectId}")
    Long getProjectId();
    @Value("#{target.project.name}")
    String getProjectName();

    @Value("#{target.user.userId}")
    Long getUserId();
    @Value("#{target.user.firstName}")
    String getFirstName();
    @Value("#{target.user.lastName}")
    String getLastName();
}
