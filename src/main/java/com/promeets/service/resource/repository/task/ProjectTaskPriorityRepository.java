package com.promeets.service.resource.repository.task;

import com.promeets.service.resource.model.entity.task.ProjectTaskPriority;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(
        path = "task-priorities",
        collectionResourceRel = "task-priorities",
        itemResourceRel = "task-priority"
)
public interface ProjectTaskPriorityRepository extends CrudRepository<ProjectTaskPriority, Long> {
}
