package com.promeets.service.resource.repository.project;

import com.promeets.service.resource.model.entity.project.Project;
import com.promeets.service.resource.model.entity.project.UserProject;
import com.promeets.service.resource.model.entity.security.SecurityRoles;
import com.promeets.service.resource.model.service.security.UserSecurityService;
import com.promeets.service.resource.repository.organization.OrganizationRepository;
import com.promeets.service.resource.repository.security.SecurityRoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.core.annotation.HandleAfterCreate;
import org.springframework.data.rest.core.annotation.HandleBeforeCreate;
import org.springframework.data.rest.core.annotation.RepositoryEventHandler;
import org.springframework.stereotype.Component;

@Component
@RepositoryEventHandler(Project.class)
public class ProjectEventHandler {

    private final UserSecurityService userSecurityService;
    private final UserProjectRepository userProjectRepository;
    private final SecurityRoleRepository securityRoleRepository;
    private final OrganizationRepository organizationRepository;

    @Autowired
    public ProjectEventHandler(UserProjectRepository userProjectRepository, UserSecurityService userSecurityService,
                               SecurityRoleRepository securityRoleRepository, OrganizationRepository organizationRepository) {
        this.userProjectRepository = userProjectRepository;
        this.userSecurityService = userSecurityService;
        this.securityRoleRepository = securityRoleRepository;
        this.organizationRepository = organizationRepository;
    }

    @HandleBeforeCreate
    public void handleBeforeCreate(Project project) {
        project.setOrganization(organizationRepository.findOne(1L)); // for testing purposes
    }

    @HandleAfterCreate
    public void handleAfterCreate(Project project) {
        UserProject userProject = new UserProject();
        userProject.setUser(userSecurityService.currentUser());
        userProject.setProject(project);
        userProject.setRole(securityRoleRepository.findByName(SecurityRoles.ROLE_OWNER.name()));
        userProjectRepository.save(userProject);
    }
}
