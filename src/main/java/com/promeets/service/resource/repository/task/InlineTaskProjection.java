package com.promeets.service.resource.repository.task;

import com.promeets.service.resource.model.entity.task.ProjectTask;
import com.promeets.service.resource.model.entity.task.ProjectTaskPriority;
import com.promeets.service.resource.model.entity.task.ProjectTaskStatus;
import com.promeets.service.resource.model.entity.user.User;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

import java.sql.Timestamp;

@Projection(name = "inlineTask", types = {ProjectTask.class})
public interface InlineTaskProjection {
    @Value("#{target.taskId}")
    long getTaskId();

    @Value("#{target.name}")
    String getName();

    @Value("#{target.description}")
    String getDescription();

    @Value("#{target.createdAt}")
    Timestamp getCreatedAt();

    @Value("#{target.dueDate}")
    Timestamp getDueDate();

    @Value("#{target.taskPriority}")
    ProjectTaskPriority getTaskPriority();

    @Value("#{target.taskStatus}")
    ProjectTaskStatus getTaskStatus();

    @Value("#{target.reporter}")
    User getReporter();

    @Value("#{target.assignee}")
    User getAssignee();
}
