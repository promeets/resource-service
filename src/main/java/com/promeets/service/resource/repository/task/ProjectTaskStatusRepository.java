package com.promeets.service.resource.repository.task;

import com.promeets.service.resource.model.entity.task.ProjectTaskStatus;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(
        path = "task-statuses",
        collectionResourceRel = "task-statuses",
        itemResourceRel = "task-status"
)
public interface ProjectTaskStatusRepository extends CrudRepository<ProjectTaskStatus, Long> {
}
