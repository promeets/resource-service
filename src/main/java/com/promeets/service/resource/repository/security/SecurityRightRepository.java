package com.promeets.service.resource.repository.security;

import com.promeets.service.resource.model.entity.security.SecurityRight;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(
        path = "security-rights",
        collectionResourceRel = "security-rights",
        itemResourceRel = "security-right"
)
public interface SecurityRightRepository extends CrudRepository<SecurityRight, Long> {
}

