package com.promeets.service.resource.repository.task;

import com.promeets.service.resource.model.entity.security.SecurityRoles;
import com.promeets.service.resource.model.entity.task.ProjectTask;
import com.promeets.service.resource.model.entity.task.UserProjectTask;
import com.promeets.service.resource.model.service.security.UserSecurityService;
import com.promeets.service.resource.repository.security.SecurityRoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.core.annotation.HandleAfterCreate;
import org.springframework.data.rest.core.annotation.HandleBeforeCreate;
import org.springframework.data.rest.core.annotation.RepositoryEventHandler;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.time.Instant;

@Component
@RepositoryEventHandler(ProjectTask.class)
public class ProjectTaskEventHandler {

    private final UserSecurityService userSecurityService;
    private final SecurityRoleRepository securityRoleRepository;
    private final UserProjectTaskRepository userProjectTaskRepository;
    private final ProjectTaskStatusRepository projectTaskStatusRepository;

    @Autowired
    public ProjectTaskEventHandler(UserSecurityService userSecurityService,
                                   SecurityRoleRepository securityRoleRepository,
                                   UserProjectTaskRepository userProjectTaskRepository,
                                   ProjectTaskStatusRepository projectTaskStatusRepository) {
        this.userSecurityService = userSecurityService;
        this.securityRoleRepository = securityRoleRepository;
        this.userProjectTaskRepository = userProjectTaskRepository;
        this.projectTaskStatusRepository = projectTaskStatusRepository;
    }

    @HandleBeforeCreate
    public void handleBeforeCreate(ProjectTask task) {
        task.setCreatedAt(Timestamp.from(Instant.now()));
        task.setReporter(userSecurityService.currentUser());
        task.setTaskStatus(projectTaskStatusRepository.findOne(1L));// Set status to Open
    }

    @HandleAfterCreate
    public void handleAfterCreate(ProjectTask task) {
        UserProjectTask userProjectTask = new UserProjectTask();
        userProjectTask.setTask(task);
        userProjectTask.setUser(userSecurityService.currentUser());
        userProjectTask.setRole(securityRoleRepository.findByName(SecurityRoles.ROLE_OWNER.name()));
        userProjectTaskRepository.save(userProjectTask);
    }
}
