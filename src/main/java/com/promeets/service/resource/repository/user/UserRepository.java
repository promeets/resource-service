package com.promeets.service.resource.repository.user;

import com.promeets.service.resource.model.entity.user.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RestResource;

import java.util.Optional;

public interface UserRepository extends PagingAndSortingRepository<User, Long> {

    Optional<User> findByEmail(@Param("email") String email);

    @RestResource(rel = "findByName", path = "findByName")
    Page<User> findByFirstNameContainsOrLastNameContains(
            @Param("firstName") String firstName,
            @Param("lastName") String lastName,
            Pageable pageable
    );

    @Query("select u from User u where u.userId = ?#{currentUser.userId}")
    @RestResource(path = "me", rel = "me")
    User me();

}
