package com.promeets.service.resource.repository.organization;

import com.promeets.service.resource.model.entity.organization.Organization;
import com.promeets.service.resource.model.entity.organization.UserOrganization;
import com.promeets.service.resource.model.entity.user.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import java.util.Optional;

@RepositoryRestResource(
        path = "user-organizations",
        collectionResourceRel = "user-organizations",
        itemResourceRel = "user-organization"
)
public interface UserOrganizationRepository extends CrudRepository<UserOrganization, Long> {

    @RestResource(path = "find", rel = "find")
    Optional<UserOrganization> findByUserUserIdAndOrganizationOrganizationId(
            @Param("userId") long userId,
            @Param("organizationId") long organizationId
    );

    @RestResource(exported = false)
    Optional<UserOrganization> findByUserAndOrganization(
            @Param("User") User user,
            @Param("Organization") Organization organization
    );

}
