package com.promeets.service.resource.repository.project;

import com.promeets.service.resource.model.entity.project.UserProject;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(
        path = "user-projects",
        collectionResourceRel = "user-projects",
        itemResourceRel = "user-project",
        excerptProjection = InlineProjectProjection.class
)
public interface UserProjectRepository extends CrudRepository<UserProject, Long> {
}
