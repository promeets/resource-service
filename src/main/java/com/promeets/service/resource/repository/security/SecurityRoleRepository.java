package com.promeets.service.resource.repository.security;

import com.promeets.service.resource.model.entity.security.SecurityRole;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(
        path = "security-roles",
        collectionResourceRel = "security-roles",
        excerptProjection = RoleProjection.class
)
public interface SecurityRoleRepository extends CrudRepository<SecurityRole, Long> {
    SecurityRole findByName(@Param("name") String name);
}
