package com.promeets.service.resource.repository.task;

import com.promeets.service.resource.model.entity.task.UserProjectTask;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(
        path = "user-tasks",
        collectionResourceRel = "user-tasks",
        itemResourceRel = "user-task"
)
public interface UserProjectTaskRepository extends PagingAndSortingRepository<UserProjectTask, Long> {
}
