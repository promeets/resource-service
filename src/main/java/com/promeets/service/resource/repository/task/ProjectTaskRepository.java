package com.promeets.service.resource.repository.task;

import com.promeets.service.resource.model.entity.task.ProjectTask;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(
        path = "project-tasks",
        collectionResourceRel = "project-tasks",
        itemResourceRel = "project-task",
        excerptProjection = InlineTaskProjection.class
)
public interface ProjectTaskRepository extends PagingAndSortingRepository<ProjectTask, Long> {
}
