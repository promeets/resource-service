package com.promeets.service.resource.repository.security;

import com.promeets.service.resource.model.entity.security.SecurityRight;
import com.promeets.service.resource.model.entity.security.SecurityRole;
import org.springframework.data.rest.core.config.Projection;

import java.util.List;

@Projection(name = "inlineRights", types = SecurityRole.class)
public interface RoleProjection {
    List<SecurityRight> getRights();
}
