package com.promeets.service.resource.repository.organization;

import com.promeets.service.resource.model.entity.organization.Organization;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface OrganizationRepository extends CrudRepository<Organization, Long> {
    //stay here as an example
    @Override
    /*@PreAuthorize("@userOrganizationSecurityService.canRead(#id)")*/
    Organization findOne(@Param("id") Long id);
}
