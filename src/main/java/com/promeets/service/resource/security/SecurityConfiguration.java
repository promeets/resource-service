package com.promeets.service.resource.security;

import com.promeets.service.resource.model.service.security.UserSecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;

import javax.servlet.http.HttpServletResponse;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
@Profile({"security"})
@Order(Ordered.HIGHEST_PRECEDENCE)
public class SecurityConfiguration extends AbstractSecurityConfiguration {

    @Autowired
    public SecurityConfiguration(UserSecurityService userSecurityService, DefaultUserDetailsService userDetailsService) {
        super(userSecurityService, userDetailsService);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .httpBasic()
                .authenticationEntryPoint((request, response, authException) -> response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Unauthorized"))
                .and()
                //ToDo: must be set by authentication request
                .rememberMe().alwaysRemember(true).and()
                .logout().logoutUrl("/auth/logout")
                .and()
                .csrf().csrfTokenRepository(CookieCsrfTokenRepository.withHttpOnlyFalse())
                .and()
                .authorizeRequests().anyRequest().authenticated();
    }
}