package com.promeets.service.resource.security;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties(prefix = "security.open")
public class OpenSecurityProperties {
    private String email;
    private String password;
}
