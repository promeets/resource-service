package com.promeets.service.resource.security;

import com.promeets.service.resource.model.entity.user.User;
import com.promeets.service.resource.model.service.security.UserSecurityService;
import org.springframework.context.annotation.Bean;
import org.springframework.data.repository.query.spi.EvaluationContextExtension;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

public abstract class AbstractSecurityConfiguration extends WebSecurityConfigurerAdapter {
    protected final UserSecurityService userSecurityService;
    protected final DefaultUserDetailsService userDetailsService;

    protected AbstractSecurityConfiguration(UserSecurityService userSecurityService, DefaultUserDetailsService userDetailsService) {
        this.userSecurityService = userSecurityService;
        this.userDetailsService = userDetailsService;
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth
                .userDetailsService(this.userDetailsService)
                .passwordEncoder(User.PASSWORD_ENCODER);
    }

    @Bean
    EvaluationContextExtension securityExtension() {
        return new SecurityEvaluationContextExtension();
    }

    @Bean
    EvaluationContextExtension userSecurityExtension() {
        return new UserSecurityEvaluationContextExtension(userSecurityService);
    }

}
