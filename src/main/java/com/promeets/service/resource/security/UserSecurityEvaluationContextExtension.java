package com.promeets.service.resource.security;

import com.promeets.service.resource.model.entity.user.User;
import com.promeets.service.resource.model.service.security.UserSecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.spi.EvaluationContextExtensionSupport;
import org.springframework.security.access.expression.SecurityExpressionRoot;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

public class UserSecurityEvaluationContextExtension extends EvaluationContextExtensionSupport {

    private final UserSecurityService userSecurityService;

    @Autowired
    public UserSecurityEvaluationContextExtension(UserSecurityService userSecurityService) {
        this.userSecurityService = userSecurityService;
    }

    @Override
    public String getExtensionId() {
        return "currentUser";
    }

    @Override
    public User getRootObject() {
        return userSecurityService.currentUser();
    }
}
