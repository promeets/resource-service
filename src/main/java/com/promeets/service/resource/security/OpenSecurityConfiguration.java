package com.promeets.service.resource.security;

import com.promeets.service.resource.model.service.security.UserSecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;

import javax.servlet.http.HttpServletResponse;

@Configuration
@EnableConfigurationProperties(OpenSecurityProperties.class)
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
@Profile("!security")
public class OpenSecurityConfiguration extends AbstractSecurityConfiguration {

    private final OpenSecurityProperties openSecurityProperties;

    @Autowired
    public OpenSecurityConfiguration(UserSecurityService userSecurityService, DefaultUserDetailsService userDetailsService, OpenSecurityProperties openSecurityProperties) {
        super(userSecurityService, userDetailsService);
        this.openSecurityProperties = openSecurityProperties;
    }


    @Override
    protected void configure(HttpSecurity http) throws Exception {
        String welcomeText = "Use default credentials: "
                + openSecurityProperties.getEmail()
                + " : " + openSecurityProperties.getPassword();
        http
                .httpBasic()
                .authenticationEntryPoint((request, response, e) -> {
                    response.addHeader("WWW-Authenticate", "Basic realm=\"" + welcomeText + "\"");
                    response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                    response.getWriter().write(welcomeText);
                }).and()
                .rememberMe().alwaysRemember(true).and()
                .logout().logoutUrl("/auth/logout")
                .and()
                .csrf().csrfTokenRepository(CookieCsrfTokenRepository.withHttpOnlyFalse())
                .and()
                .authorizeRequests().anyRequest().authenticated();
    }
}