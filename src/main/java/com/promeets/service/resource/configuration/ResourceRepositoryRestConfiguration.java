package com.promeets.service.resource.configuration;


import com.promeets.service.resource.model.entity.task.ProjectTaskPriority;
import com.promeets.service.resource.model.entity.task.ProjectTaskStatus;
import com.promeets.service.resource.model.entity.user.User;
import com.promeets.service.resource.repository.project.ProjectEventHandler;
import com.promeets.service.resource.repository.task.InlineTaskProjection;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurerAdapter;

@Configuration
public class ResourceRepositoryRestConfiguration extends RepositoryRestConfigurerAdapter {

    @Override
    public void configureRepositoryRestConfiguration(RepositoryRestConfiguration config) {
        super.configureRepositoryRestConfiguration(config);
        config.getProjectionConfiguration().addProjection(InlineTaskProjection.class);
        config.exposeIdsFor(ProjectTaskPriority.class, ProjectTaskStatus.class, User.class);
    }
}
