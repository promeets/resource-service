package com.promeets.service.resource.conrtoller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.view.RedirectView;

public interface UserController {
    @RequestMapping(method = RequestMethod.GET, path = "/api/user")
    RedirectView redirectToCurrentUser();
}
