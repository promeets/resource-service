package com.promeets.service.resource.conrtoller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

@RestController
@RequestMapping("/auth")
public class AuthController {

    @RequestMapping("/login")
    public Principal login(Principal user) {
        return user;
    }

}