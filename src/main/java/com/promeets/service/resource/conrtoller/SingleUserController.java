package com.promeets.service.resource.conrtoller;

import com.promeets.service.resource.model.service.security.UserSecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.view.RedirectView;

@RestController
@Profile("single")
public class SingleUserController implements UserController {
    private final UserSecurityService userSecurityService;

    @Autowired
    public SingleUserController(UserSecurityService userSecurityService) {
        this.userSecurityService = userSecurityService;
    }

    @Override
    public RedirectView redirectToCurrentUser() {
        return new RedirectView("/api/users/" + userSecurityService.currentUser().getUserId());
    }
}
